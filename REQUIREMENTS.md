### Requirements

We need API acces to 2 types of data from all 3 Norde vehicle brands:

- https://www.norde.lv/nissan
- https://renault.norde.lv/
- https://dacia.norde.lv/

From each of tehe pages we need. All data in example is what we need, but the names of variables are fully up to you. Also if there is some deviation from the format or data structure - it's ok, as long as the information we need is somehow accessable.

#### News:

- **News list**. Required data example in json:

```js
            [
                ..
                {
                    id: 12, // News item unique ID in your DB
                    published_at: "2019-02-01T03:45:27.612584Z", // Time format is fully up to you, but pleae provide in what format
                    img_url: "https://www.norde.lv/static/400x/upload/Jaunumu_sadala/DSCF0226.png",
                    title: "Stilizēta ūdenspīpe, f1 pilota tērps ar autogrāfu, luksus galda futbols un citi neparastākie auto zīmolu",
                    excerpt: "Visgrūtāk pārsteigumu Ziemassvētkos atrast tiem, kuriem šķietami viss ir, tomēr nekas neiepriecinās īstenu auto entuziastu kā dāvana, kas apliecinās šīs personas patiku pret savu spēkratu. Vēl labāk, ja tas ir kāds īpašs", // Can be HTML formated if need be..
                    url: "https://www.norde.lv/info-par-nissan/jaunumi/show/news/752171/Stiliz-ta-densp-pe-F1-pilota-t-rps-ar-autogr-fu-luksus-galda-futbols-un-citi-neparast-kie-auto-z-molu-rad-tie-produkti",
                },
                {
                    id: 13, // News item unique ID in your DB
                    published_at: "2019-02-01T03:45:27.612584Z", // Time format is fully up to you, but pleae provide in what format
                    img_url: "https://www.norde.lv/static/400x/upload/Jaunumu_sadala/DSCF0226.png",
                    title: "Stilizēta ūdenspīpe, f1 pilota tērps ar autogrāfu, luksus galda futbols un citi neparastākie auto zīmolu",
                    excerpt: "Visgrūtāk pārsteigumu Ziemassvētkos atrast tiem, kuriem šķietami viss ir, tomēr nekas neiepriecinās īstenu auto entuziastu kā dāvana, kas apliecinās šīs personas patiku pret savu spēkratu. Vēl labāk, ja tas ir kāds īpašs", // Can be HTML formated if need be..
                    url: "https://www.norde.lv/info-par-nissan/jaunumi/show/news/752171/Stiliz-ta-densp-pe-F1-pilota-t-rps-ar-autogr-fu-luksus-galda-futbols-un-citi-neparast-kie-auto-z-molu-rad-tie-produkti",
                },
                ..
            ]
```

- **News item** by id (your DB id)

```js
            {
                id: 12, // News item unique ID in your DB
                published_at: "2019-02-01T03:45:27.612584Z", // Time format is fully up to you, but pleae provide in what format
                img_url: "https://www.norde.lv/static/400x/upload/Jaunumu_sadala/DSCF0226.png",
                title: "Stilizēta ūdenspīpe, f1 pilota tērps ar autogrāfu, luksus galda futbols un citi neparastākie auto zīmolu",
                excerpt: "Visgrūtāk pārsteigumu Ziemassvētkos atrast tiem, kuriem šķietami viss ir, tomēr nekas neiepriecinās īstenu auto entuziastu kā dāvana, kas apliecinās šīs personas patiku pret savu spēkratu. Vēl labāk, ja tas ir kāds īpašs", // Can be HTML formated if need be..
                url: "https://www.norde.lv/info-par-nissan/jaunumi/show/news/752171/Stiliz-ta-densp-pe-F1-pilota-t-rps-ar-autogr-fu-luksus-galda-futbols-un-citi-neparast-kie-auto-z-molu-rad-tie-produkti",
            }
```

#### Offers (both Service offers and Regular offers).. Basically data from these pages:

(Nissan - https://www.norde.lv/akcijas/visas-akcijas; Renault - https://renault.norde.lv/akcijas/visas-akcijas; Dacia - https://dacia.norde.lv/akcijas/visas-akcijas)




- **Offers list**:

```js
            [
                ..
                {
                    id: 12, // item unique ID in your DB
                    published_at: "2019-02-01T03:45:27.612584Z", // Time format is fully up to you, but pleae provide in what format
                    img_url: "https://www.norde.lv/upload/Servisa_akcijas/servisa_ziemas_servisa_akcija.jpg",
                    title: "Īpašie piedāvājumi nissan klientiem",
                    url: "https://www.norde.lv/lietotajiem/ipasie-piedavajumi-nissan-serviss",
                },
                {
                    id: 13, // item unique ID in your DB
                    published_at: "2019-02-01T03:45:27.612584Z", // Time format is fully up to you, but pleae provide in what format
                    img_url: "https://www.norde.lv/upload/Servisa_akcijas/servisa_ziemas_servisa_akcija.jpg",
                    title: "Īpašie piedāvājumi nissan klientiem",
                    url: "https://www.norde.lv/lietotajiem/ipasie-piedavajumi-nissan-serviss",
                },
                ..
            ]
```

- **Offers item**:

```js
            {
                id: 12, // item unique ID in your DB
                published_at: "2019-02-01T03:45:27.612584Z", // Time format is fully up to you, but pleae provide in what format
                img_url: "https://www.norde.lv/upload/Servisa_akcijas/servisa_ziemas_servisa_akcija.jpg",
                title: "Īpašie piedāvājumi nissan klientiem",
                url: "https://www.norde.lv/lietotajiem/ipasie-piedavajumi-nissan-serviss",
            }
```
